#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# File              : distance.py
# Author            : FND <fndemers@protonmail.com>
# Date              : 20.12.2022
# Last Modified Date: 23.01.2023
# Last Modified By  : FND <fndemers@protonmail.com>
# -*- coding: utf-8 -*-
# distance.py
# Copyright (c) 2022 FND <fndemers@protonmail.com>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import sys

from Levenshtein import *

monRepertoire = '.'

from os import walk
listeFichiers = []
for (repertoire, sousRepertoires, fichiers) in walk(monRepertoire):
    # Trouve le répertoire de la liste des fichiers...
    suffix = repertoire.removeprefix(monRepertoire)
    # On s'assure que le dossier n'est pas un dossier caché (commençant par .)
    if len(suffix) == 0 or len(suffix) > 0 and suffix[1] != '.':
        # On enlève les fichiers qui sont cachés (commençant par .)
        nouvelle_liste_fichiers = [ fichier for fichier in fichiers if fichier[0] != '.']
        listeFichiers.extend([repertoire + '/' + element_fichier for element_fichier in nouvelle_liste_fichiers])

print(listeFichiers)

# Pour trier une liste avec tuples
# On place l'élément à trier au début du tuple
# sorted(liste, key=lambda x: (x[1], x[0]))
# sorted(liste, key=lambda x: (x[1], x[0]), reverse=True)
#

# txt1 = open(sys.argv[1]).read()
# txt2 = open(sys.argv[2]).read()

# # print("distance:", distance(txt1,txt2))
# print("ratio:", (1.0 - ratio(txt1,txt2))* 100)