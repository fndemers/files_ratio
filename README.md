# Comparaison de fichiers par pourcentage


## Questions sur Internet

https://superuser.com/questions/347560/is-there-a-tool-to-measure-file-difference-percentage

## Théorie

Il existe déjà un système de comparaison de code appelé [Moss](https://theory.stanford.edu/~aiken/moss/) mais il est dépendant d'un serveur pour réaliser les comparaisons.

Ici, nous utiliserons le calcul de la distance de Levenshtein https://en.wikipedia.org/wiki/Levenshtein_distance

## Librairie Python

La librairie utilisée est https://pypi.org/project/python-Levenshtein/

### Documentation

https://maxbachmann.github.io/Levenshtein/levenshtein.html

