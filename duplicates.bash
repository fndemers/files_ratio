#!/bin/bash
# File              : duplicates.bash
# Author            : FND <fndemers@protonmail.com>
# Date              : 20.12.2022
# Last Modified Date: 21.12.2022
# Last Modified By  : FND <fndemers@protonmail.com>

NOMBRE_PARAMETRES=1


if [[ $# -ne $NOMBRE_PARAMETRES ]]
then
    echo "ERREUR: Pas suffisamment de paramètres."
    echo "Paramètre 1: Fichier CSV des URLs Git"

	read -p "Installation des paquetages initiaux? (o/n) [n]" CHOIX
	if [[ $CHOIX =~ ^[Oo]$ ]]
	then
		sudo apt update
		sudo apt install -y fdupes

	fi
    exit 0
fi

FICHIERCSV=$1

while read -r line
do
    echo "$line"
done < "$FICHIERCSV"
exit 1


cd /tmp
GITNAME=$(basename -s .git "$GITURL")

read -p "Mise à jour des dépôts localement? (o/n) [n]" CHOIX
if [[ $CHOIX =~ ^[Oo]$ ]]
then
    rm -f -r "/tmp/$GITNAME"
    git clone "$GITURL"
fi
